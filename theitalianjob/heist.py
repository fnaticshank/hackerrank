"""
NOTE: This is a Dynamic programming problem

https://www.hackerrank.com/challenges/the-indian-job

Input Format:
The first line contains an integer T denoting the number of testcases.  T
testcases follow. Each testcase consists of two lines. First line contains two
space separated integers denoting N and G denoting the number of thieves and
duration for which guard leaves the museum.  The next line contains N space
separated numbers where the ith integer, A[i] represents the time the ith
robber needs to be in the vault.

Sample Input:
2           T
3 4         N, G
2 4 2       N number of theives
3 2
2 4 2

Sample Output:
YES
No

Solution status: Failing
"""

def heist():
    T = int(raw_input().strip())
    testcases = []
    for t in range(T):
        N, G = map(int, raw_input().strip().split())
        thieves = map(int, raw_input().strip().split())
        testcases.append(((N, G), thieves))
    ## now we have the testcases
    for t in testcases:
        initiate_heist(t)
    #return testcases

def initiate_heist(t):
    N, G = t[0]
    thieves = t[1]

    if all(thief_time <= G for thief_time in thieves):
        totaltime = reduce(lambda x,y: x +y, thieves)
        avg_time = totaltime/2
        if avg_time <= G:
            print 'YES'
        else:
            print 'NO'
    else:
        print 'NO'

if __name__ == '__main__':
    heist()
